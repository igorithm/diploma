//package com.igorv.careerAnalysis;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import com.gargoylesoftware.htmlunit.WebClient;
//import com.gargoylesoftware.htmlunit.html.HtmlDivision;
//import com.gargoylesoftware.htmlunit.html.HtmlForm;
//import com.gargoylesoftware.htmlunit.html.HtmlPage;
//import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
//import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
//import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
//
///**
// * A simple test using HtmlUnit.
// * 
// * @author Igor Vinojcic
// */
//public class HtmlUnitTest {
//	static private final WebClient browser;
//
//	static {
//		browser = new WebClient();
//	}
//
//	public static void main(String[] arguments) {
//		// boolean result;
//		try {
//			// result = searchTest();
//			loginTest();
//		} catch (Exception e) {
//			e.printStackTrace();
//			// result = false;
//		}
//
//		// System.out.println("Test " + (result ? "passed." : "failed."));
//		// if (!result) {
//		// System.exit(1);
//		// }
//	}
//
//	public static void loginTest() {
//		try {
//
//			/*
//			 * Set webClient options. LinkedIn has some javascript issues so we
//			 * will disable javascript
//			 */
//			browser.getOptions().setJavaScriptEnabled(false);
//			// browser.setUseInsecureSSL(true);
//			// browser.setRedirectEnabled(true);
//			// browser.setCookiesEnabled(true);
//
//			final HtmlPage page = (HtmlPage) browser.getPage("https://www.linkedin.com/uas/login");
//
//			final HtmlForm form = (HtmlForm) page.getForms().get(0);
//			final HtmlSubmitInput button = (HtmlSubmitInput) form.getInputByValue("Sign In");
//			final HtmlTextInput emailBtn = (HtmlTextInput) form.getInputByName("session_key");
//			final HtmlPasswordInput passBtn = (HtmlPasswordInput) form.getInputByName("session_password");
//
//			// Change the value of the text field
//			emailBtn.setValueAttribute("igorvinojcic@gmail.com");
//			passBtn.setValueAttribute("d12m20i77");
//
//			// Now submit the form by clicking the button and get back the
//			// second page
//			final HtmlPage page2 = (HtmlPage) button.click();
//
//			System.out.println("==========================================");
//			System.out.println(page2.getWebResponse().getContentAsString());
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
//
//	private static boolean searchTest() {
//		HtmlPage currentPage;
//
//		String xpath = "//label[contains(@for, 'session_key-login')]";
//		final HtmlDivision fullName;
//
//		try {
//			currentPage = (HtmlPage) browser.getPage("http://www.linkedin.com");
//		} catch (Exception e) {
//			System.out.println("Could not open browser window");
//			e.printStackTrace();
//			return false;
//		}
//		System.out.println("Simulated browser opened.");
//
//		try {
//			fullName = (HtmlDivision) currentPage.getByXPath(xpath).get(0);
//
//			System.out.println(fullName.getNodeValue());
//			return true;
//
//		} catch (Exception e) {
//			System.out.println("Could not search");
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	public static boolean containsPattern(String string, String regex) {
//		Pattern pattern = Pattern.compile(regex);
//
//		// Check for the existence of the pattern
//		Matcher matcher = pattern.matcher(string);
//		return matcher.find();
//	}
//}