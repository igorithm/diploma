package com.igorv.careerAnalysis.eprints.serialization;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlElement;

public class Eprint {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

	@XmlElement(name = "eprintid")
	private int eprintId;

	@XmlElement(name = "rev_number")
	private String revNumber;

	@XmlElement(name = "userid")
	private int userId;

	@XmlElement(name = "datestamp")
	private String dateStamp;

	@XmlElement(name = "lastmod")
	private String lastMod;

	@XmlElement(name = "type")
	private String type;

	/* First and last name of thesis creator */
	@XmlElement(name = "creators")
	private Creators creators;

	/* Titles of thesis in different languages */
	@XmlElement(name = "title")
	private Title title;

	/* Keywords of thesis in different languages */
	@XmlElement(name = "keywords")
	private Keywords keywords;

	@XmlElement(name = "date")
	private String date;

	/* For example: Submitted. Look for date variable */
	@XmlElement(name = "date_type")
	private String date_type;

	/* Time of Oral exam */
	@XmlElement(name = "oral_time")
	private String oral_time;

	/* uni, vsš */
	@XmlElement(name = "study_type")
	private String study_type;

	/* Name of the mentor */
	@XmlElement(name = "mentors")
	private Mentors mentors;

	public int getEprintId() {
		return eprintId;
	}

	public String getRevNumber() {
		return revNumber;
	}

	public int getUserId() {
		return userId;
	}

	public String getDateStamp() {
		return dateStamp;
	}

	public String getLastmod() {
		return lastMod;
	}

	public String getType() {
		return type;
	}

	public Creators getCreators() {
		return creators;
	}

	public Title getTitle() {
		return title;
	}

	public Keywords getKeywords() {
		return keywords;
	}

	public String getDateString() {
		return date;
	}

	public Date getDate() {

		try {
			return dateFormat.parse(this.getDateString());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getDateYear() {

		if (this.getDate() == null) {
			return null;
		}
		return yearFormat.format(this.getDate());
	}

	public String getDateYearMinusOne() {

		if (this.getDate() == null) {
			return null;
		}

		Calendar cal = new GregorianCalendar();
		cal.setTime(this.getDate());

		/* Substract 1 */
		cal.add(Calendar.YEAR, -1);

		return yearFormat.format(cal.getTime());
	}

	public String getDate_type() {
		return date_type;
	}

	public String getStudy_type() {
		return study_type;
	}

	public Mentors getMentors() {
		return mentors;
	}

	public String getLastMod() {
		return lastMod;
	}

	public String getOralDateTimeString() {
		return oral_time;
	}

	public Date getOralDate() {

		try {
			return dateFormat.parse(this.getOralDateTimeString());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}

	public String getOralDateString() {

		if (this.getOralDate() == null) {
			return null;
		}

		return dateFormat.format(this.getOralDate());
	}

	public String getOralDateYear() {

		if (this.getOralDate() == null) {
			return null;
		}

		return yearFormat.format(this.getOralDate());
	}

	public String getOralDateYearMinusOne() {

		if (this.getOralDate() == null) {
			return null;
		}

		Calendar cal = new GregorianCalendar();
		cal.setTime(this.getOralDate());

		/* Substract 1 */
		cal.add(Calendar.YEAR, -1);

		return yearFormat.format(cal.getTime());
	}

	@Override
	public String toString() {
		String result = "Creators{" + "creators=" + creators + '}' + "\n";
		result = result + creators.toString();

		return result;
	}

}
