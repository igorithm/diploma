package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Keywords {

	/* Same keywords in different languages */
	@XmlElement(name = "item")
	private List<Item> items;

	public List<Item> getItems() {
		return items;
	}

	public String[] getKeywordsAng() {

		String[] keywords = null;
		for (Item item : getItems()) {
			/* only ang keywords */
			if (item.getLang().equalsIgnoreCase("en")) {
				keywords = item.getName().trim().split(",");
				break;
			}
		}
		return keywords;
	}
}
