/**
 * 
 */
/**
 * @author igor
 *
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://eprints.org/ep2/data/2.0", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.igorv.careerAnalysis.eprints.serialization;