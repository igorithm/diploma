package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Title {

	@XmlElement(name = "item")
	private List<Item> items;

	public List<Item> getItems() {
		return items;
	}

	public String getTitleSlo() {

		for (Item item : getItems()) {
			if (item.getLang().equalsIgnoreCase("sl")) {
				return item.getName();
			}
		}

		return null;
	}

	public String getTitleAng() {

		for (Item item : getItems()) {
			if (item.getLang().equalsIgnoreCase("en")) {
				return item.getName();
			}
		}

		return null;
	}

}
