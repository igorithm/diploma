package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Mentors {

	@XmlElement(name = "item")
	private List<ItemPerson> items;

	public List<ItemPerson> getItems() {
		return items;
	}

	public ItemPerson getMentor() {

		for (ItemPerson person : getItems()) {
			if (person.getFunction().equalsIgnoreCase("mentor")) {
				return person;
			}
		}

		return items.get(0);
	}
}
