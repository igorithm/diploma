package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Creators {

	@XmlElement(name = "item")
	private List<ItemPerson> items;

	public List<ItemPerson> getItems() {
		return items;
	}

	@Override
	public String toString() {
		String result = "List<Item>{" + "items=" + items + '}' + "\n";

		for (ItemPerson item : items) {
			result = result + item.toString();
		}

		return result;
	}

}
