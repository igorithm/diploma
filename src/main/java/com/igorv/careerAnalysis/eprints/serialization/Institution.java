package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Institution {

	@XmlElement(name = "item")
	private List<ItemPerson> items;

	public List<ItemPerson> getItems() {
		return items;
	}

}
