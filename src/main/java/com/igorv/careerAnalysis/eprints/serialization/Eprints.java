package com.igorv.careerAnalysis.eprints.serialization;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "eprints")
@XmlType(name = "")
public class Eprints {

	public static String EPRINTS_URL = "http://eprints.fri.uni-lj.si/cgi/search/advanced/export_fri_XML.xml?"
			+ "screen=Public%3A%3AEPrintSearch&_action_export=1&output=XML"
			+ "&exp=0%7C1%7C-date%2Fcreators_name%2Ftitle%7Carchive%7C-%7C"
			+ "thesis_type%3Athesis_type%3AANY%3AEQ%3Aengd%7Ctype%3Atype%3A"
			+ "ANY%3AEQ%3Athesis%7C-%7Ceprint_status%3Aeprint_status%3AALL%3"
			+ "AEQ%3Aarchive%7Cmetadata_visibility%3Ametadata_visibility%3AAL" + "L%3AEX%3Ashow&n=&cache=898634";

	@XmlElement(name = "eprint")
	private List<Eprint> eprints;

	public List<Eprint> getEprints() {
		return eprints;
	}

	@Override
	public String toString() {
		String result = "List<Eprint>{" + "eprints=" + eprints + '}' + "\n";

		for (Eprint eprint : eprints) {
			result = result + eprint.toString();
		}

		return result;
	}

}
