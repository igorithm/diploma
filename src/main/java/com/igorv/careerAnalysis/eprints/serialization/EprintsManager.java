package com.igorv.careerAnalysis.eprints.serialization;

import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class EprintsManager {

	/* Eprints URL */
	private static final String URL_EPRINTS = "http://eprints.fri.uni-lj.si/cgi/search/advanced/export_fri_XML.xml?screen=Public%3A%3AEPrintSearch&_action_export=1&output=XML&exp=0%7C1%7C-date%2Fcreators_name%2Ftitle%7Carchive%7C-%7Cthesis_type%3Athesis_type%3AANY%3AEQ%3Aengd%7Ctype%3Atype%3AANY%3AEQ%3Athesis%7C-%7Ceprint_status%3Aeprint_status%3AALL%3AEQ%3Aarchive%7Cmetadata_visibility%3Ametadata_visibility%3AALL%3AEX%3Ashow&n=&cache=793663";

	public static void UnmarshallEprints() {
		try {

			/************* EPRITNS ******************/
			URL url = new URL(URL_EPRINTS);
			JAXBContext jaxbContext = JAXBContext.newInstance(Eprints.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Eprints eprints = (Eprints) jaxbUnmarshaller.unmarshal(url);
			System.out.println(eprints);
			/************* EPRITNS END ******************/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
