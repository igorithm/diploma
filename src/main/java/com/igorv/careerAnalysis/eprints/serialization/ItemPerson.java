package com.igorv.careerAnalysis.eprints.serialization;

import javax.xml.bind.annotation.XmlElement;

public class ItemPerson {

	/* Contains first and last name */
	@XmlElement(name = "name")
	private Name name;

	/* Only of this item is creator item */
	@XmlElement(name = "id")
	private int id;

	/* If item names are in different languages */
	@XmlElement(name = "lang")
	private String lang;

	/* Mentor, for example */
	@XmlElement(name = "function")
	private String function;

	public Name getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public String getLang() {
		return lang;
	}

	public String getFunction() {
		return function;
	}

	@Override
	public String toString() {
		String result = "Name{" + "name=" + name + '}' + "\n";
		result = result + name.toString();

		return result;
	}

}
