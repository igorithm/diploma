package com.igorv.careerAnalysis.eprints.serialization;

import javax.xml.bind.annotation.XmlElement;

public class Name {

	@XmlElement(name = "family")
	private String family;

	@XmlElement(name = "given")
	private String given;

	public String getFamily() {
		return family;
	}

	public String getGiven() {
		return given;
	}

	@Override
	public String toString() {
		String result = "Family{" + "family=" + family + '}';
		result = result + "Given{" + "given=" + given + '}' + "\n";

		return result;
	}

}
