package com.igorv.careerAnalysis;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.igorv.careerAnalysis.database.DatabaseManager;
import com.igorv.careerAnalysis.eprints.serialization.Eprint;
import com.igorv.careerAnalysis.eprints.serialization.Eprints;
import com.igorv.careerAnalysis.linkedIn.LinkedInExperienceModel;
import com.igorv.careerAnalysis.linkedIn.LinkedInManager;

public class CareerAnalysis {

	public static void main(String[] args) {

		retrieveAndStoreData();

	}

	/* 1. Retrieve */
	private static void retrieveAndStoreData() {

		/* Retrieve eprints data - Deserialize Eprints */
		Eprints eprints = deserializeEprints();

		/* LinkedInManager instance */
		LinkedInManager linkedInManager = new LinkedInManager(eprints);

		/* DatabaseManager instance */
		DatabaseManager databaseManager = new DatabaseManager();

		/* Retrieve LinkedIn data - parse LinkedIn */
		int stat = 0;
		int index = 0;

		System.out.println("Vseh je: " + eprints.getEprints().size());

		Connection connection = databaseManager.connectToDatabase();

		for (Eprint eprint : eprints.getEprints().subList(0, 530)) {

			ArrayList<LinkedInExperienceModel> validProfileData = linkedInManager.getValidProfileData(eprint);
			if (validProfileData != null) {
				System.out.println("Valid: YES");

				/* insert data */
				databaseManager.insertData(connection, eprint, validProfileData);

				stat++;
			} else {
				System.out.println("Valid: NOT");
			}

			System.out.println("******************* index:" + index + " *** stat: " + stat
					+ " ************************");

			index++;
		}

		databaseManager.close(connection);
	}

	/**
	 * Method responsible for deserializing Eprints XML data
	 * 
	 * @return - deserialized xml data
	 */
	private static Eprints deserializeEprints() {

		Unmarshaller jaxbUnmarshaller;
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Eprints.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Eprints eprints = (Eprints) jaxbUnmarshaller.unmarshal(new URL(Eprints.EPRINTS_URL));
			return eprints;
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
