package com.igorv.careerAnalysis.database;

public class TitleHierarchy {

	private static final String OWNER_LEVEL = "Owner";
	private static final String CORPORATE_LEVEL = "Corporate";
	private static final String SENIOR_LEVEL = "Senior";
	private static final String MIDDLE_LEVEL = "Middle";
	private static final String LOW_LEVEL = "Low";

	public static String[] getHierarchyGroupAndLevel(String title) {

		String group = ownerLevelJob(title);

		String[] result = new String[2];

		if (group != null) {
			result[0] = group;
			result[1] = OWNER_LEVEL;
			return result;
		}

		group = corporateLevelJob(title);

		if (group != null) {
			result[0] = group;
			result[1] = CORPORATE_LEVEL;
			return result;
		}

		group = seniorLevelJob(title);

		if (group != null) {
			result[0] = group;
			result[1] = SENIOR_LEVEL;
			return result;
		}

		group = middleManagementLevelJob(title);

		if (group != null) {
			result[0] = group;
			result[1] = MIDDLE_LEVEL;
			return result;
		}

		group = lowLevelJob(title);

		if (group != null) {
			result[0] = group;
			result[1] = LOW_LEVEL;
			return result;
		}

		result[0] = "other";
		result[1] = LOW_LEVEL;
		return result;
	}

	public static int getHierarchyLevel(String level) {

		if (level.equalsIgnoreCase(LOW_LEVEL)) {
			return 1;
		}
		if (level.equalsIgnoreCase(MIDDLE_LEVEL)) {
			return 2;
		}
		if (level.equalsIgnoreCase(SENIOR_LEVEL)) {
			return 3;
		}
		if (level.equalsIgnoreCase(CORPORATE_LEVEL)) {
			return 4;
		}
		if (level.equalsIgnoreCase(OWNER_LEVEL)) {
			return 4;
		}

		return -1;
	}

	private static String ownerLevelJob(String title) {

		final String[] ownerKeys = { "owner", "founder", "enterpreneur" };

		title = title.toLowerCase();

		for (String key : ownerKeys) {
			if (title.contains(key)) {
				return "Owner";
			}
		}

		return null;
	}

	private static String corporateLevelJob(String title) {

		final String[] CorporateKeys = { "chief", "president", "principal", "ceo", "cto", " cbo", "corporate", "dean",
				"vice dean" };

		title = title.toLowerCase();

		for (String key : CorporateKeys) {
			if (title.contains(key) && !title.contains("instructor") && !title.contains("contractor")
					&& !title.contains("director")) {
				return "Corporate title";
			}
		}

		return null;
	}

	private static String seniorLevelJob(String title) {

		final String[] DirectorKeys = { "head", "director", "executive" };
		final String[] SeniorKeys = { "board of", "member of council", "member of the board", "board member" };
		final String[] professorKeys = { "associate professor", "full professor", "professor" };

		title = title.toLowerCase();

		for (String key : DirectorKeys) {
			if (title.contains(key)) {
				return "Director";
			}
		}

		for (String key : SeniorKeys) {
			if (title.contains(key)) {
				return "Senior management";
			}
		}

		for (String key : professorKeys) {
			if (title.contains(key)) {
				return "Professor";
			}
		}

		if (title.contains("scientist")) {
			return "Scientist";
		}

		return null;
	}

	private static String middleManagementLevelJob(String title) {
		final String[] seniorKeys = { "senior", "manager advisor" };
		final String[] architectKeys = { "architect", "academy instructor" };
		final String[] managerKeys = { "manager", "coordinator", "officer" };
		final String[] specialistsKeys = { "expert", "specialist", "certified", "strokovni" };
		final String[] professorKeys = { "assistant professor" };

		title = title.toLowerCase();

		if (title.contains("project manager")) {
			return "Project manager";
		}

		if (title.contains("lead")) {
			return "Lead";
		}

		for (String key : seniorKeys) {
			if (title.contains(key)) {
				return "Senior";
			}
		}

		for (String key : architectKeys) {
			if (title.contains(key)) {
				return "Architect";
			}
		}

		for (String key : managerKeys) {
			if (title.contains(key) && !title.contains("webmanager")) {
				return "Manager";
			}
		}

		for (String key : specialistsKeys) {
			if (title.contains(key)) {
				return "Specialist";
			}
		}

		for (String key : professorKeys) {
			if (title.contains(key)) {
				return "Assistant professor";
			}
		}

		return null;
	}

	private static String lowLevelJob(String title) {

		final String[] consultantsKeys = { "consultant", "consulting", "advisor" };
		final String[] analystKeys = { "analyst", "monitor", "planner", "analysist" };
		final String[] adminKeys = { "administrator", "obdelava podatkov", "administrativna dela" };
		final String[] engineerKeys = { "engineer", "enginner", "enginer", "integration", "implementation",
				"integrator", "sistem" };
		final String[] technicianKeys = { "repaier", "maintenance", "maintainer", "inventory", "vzdrževanje", "servis",
				"installer" };
		final String[] developerKeys = { "developer", "programmer", "programer", "development", "code", "razvoj", "r&d" };
		final String[] technologistKeys = { "informatics", "technologist", "it department" };
		final String[] designerKeys = { "designer", "editor" };
		final String[] securityKeys = { "authentication and authorization", "security" };
		final String[] testingKeys = { "tester", "qa", "quality assurance", "quality control", "testing" };
		final String[] supportKeys = { "support", "helpdesk", "help desk", "installing equipment", "pomoč", "podpora" };
		final String[] assistantKeys = { "assistant", "asistant", "asistent", "demonstrator", "junior researcher",
				"researcher", "assistent", "research", "raziskovalec", "teacher", "teaching asistant", "učitelj",
				"investigator" };
		final String[] serviceKeys = { "salesperson", "serviceperson", "sales", "seller", "service" };
		final String[] internKeys = { "intern", "internship", "praksa", "praktikant" };

		title = title.toLowerCase();

		if (title.contains("lecturer")) {
			return "Lecturer";
		}

		if (title.contains("traine")) {
			return "Trainee";
		}

		if (title.contains("freelance")) {
			return "Freelancer";
		}

		for (String key : engineerKeys) {
			if (title.contains(key)) {
				return "Engineer";
			}
		}

		for (String key : developerKeys) {
			if (title.contains(key)) {
				return "Developer";
			}
		}

		for (String key : consultantsKeys) {
			if (title.contains(key)) {
				return "Consultant";
			}
		}

		for (String key : analystKeys) {
			if (title.contains(key)) {
				return "Analyst";
			}
		}

		for (String key : adminKeys) {
			if (title.contains(key)) {
				return "Admin";
			}
		}

		for (String key : testingKeys) {
			if (title.contains(key)) {
				return "Testing";
			}
		}

		for (String key : supportKeys) {
			if (title.contains(key)) {
				return "Support";
			}
		}

		for (String key : assistantKeys) {
			if (title.contains(key)) {
				return "Assistant";
			}
		}

		for (String key : technicianKeys) {
			if (title.contains(key)) {
				return "Technician";
			}
		}

		for (String key : technologistKeys) {
			if (title.contains(key)) {
				return "Technologist";
			}
		}

		if (title.equals("it")) {
			return "Technologist";
		}

		for (String key : securityKeys) {
			if (title.contains(key)) {
				return "Information security";
			}
		}

		for (String key : designerKeys) {
			if (title.contains(key)) {
				return "Designer";
			}
		}

		for (String key : serviceKeys) {
			if (title.contains(key)) {
				return "Service";
			}
		}

		for (String key : internKeys) {
			if (title.contains(key)) {
				return "Intern";
			}
		}

		return null;
	}

}
