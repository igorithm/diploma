package com.igorv.careerAnalysis.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.igorv.careerAnalysis.eprints.serialization.Eprint;
import com.igorv.careerAnalysis.linkedIn.LinkedInExperienceModel;

/**
 * This class takes care of data retrieval and data storing in specified mysql
 * database. This class assumes that database is already created on mysql
 * database server. Currently this class does not take responsibility of
 * creating databases and other procedures of altering databases.
 * 
 * @author Igor
 * 
 */
public class DatabaseManager {

	/* Database variables */
	private static final String T_GRADUATE = "Graduate";
	private static final String T_MENTOR = "Mentor";
	private static final String T_DEGREE_KEYWORD = "DegreeKeyword";
	private static final String T_DEGREE = "Degree";
	private static final String T_KEYWORD = "Keyword";
	private static final String T_WORK = "Work";
	private static final String T_COMPANY = "Company";
	private static final String T_POSITION = "Position";
	private static final String T_WORKGROUP = "Workgroup";

	/* Connects to dabase */
	public Connection connectToDatabase() {

		Connection connection = null;

		try {

			if (connection == null) {
				// This will load the MySQL driver, each DB has its own driver
				Class.forName("com.mysql.jdbc.Driver");
				// Setup the connection with the DB
				connection = DriverManager.getConnection("jdbc:mysql://localhost/careeranalysist?"
						+ "user=root&password=d12m20i77");
			}

			return connection;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * This method inserts data about eprint. If you want to close database
	 * connection after inserting set lastTransaction as true
	 * 
	 * @param eprint
	 * @param closeConnection
	 *            - true if you want to close connection after inserting
	 */
	public void insertData(Connection connection, Eprint eprint, ArrayList<LinkedInExperienceModel> experiences) {

		/* first sort experiences from first to last */
		experiences = sort(experiences);

		/* Get mentor data and insert it */
		String mentorGiven = eprint.getMentors().getMentor().getName().getGiven();
		String mentorFamily = eprint.getMentors().getMentor().getName().getFamily();

		int mentorId = insertMentor(connection, mentorGiven, mentorFamily);

		/* Get bachelor data and insert it */
		int degreeId = eprint.getEprintId();
		String oralDate = eprint.getOralDateTimeString();
		String titleSlo = eprint.getTitle().getTitleSlo();
		String titleAng = eprint.getTitle().getTitleAng();

		insertDegree(connection, degreeId, titleSlo, titleAng, oralDate, mentorId);

		/* Get keywords and insert it */
		String[] keywords = eprint.getKeywords().getKeywordsAng();
		int keywordId = -1;

		if (keywords != null) {
			for (String keyword : keywords) {
				keyword = keyword.trim().toLowerCase();
				keywordId = insertKeyword(connection, escapeSpecialChars(keyword));
				insertDegreeKeyword(connection, degreeId, keywordId);
			}
		}

		/* Graduate data */
		String graduateGiven = eprint.getCreators().getItems().get(0).getName().getGiven();
		String graduateFamily = eprint.getCreators().getItems().get(0).getName().getFamily();

		int graduateGeneratedId = insertGraduate(connection, graduateGiven, graduateFamily, degreeId);

		int positionGeneratedId = -1;
		int companyGeneratedId = -1;
		int groupGeneratedId = -1;

		/* Start from the end */
		int insertedWorkId = -1;
		int fkWorkNext = -1;
		boolean alreadyLast = false;
		boolean promoted = false;
		int daysNeeded = -1;
		LinkedInExperienceModel experience = null;

		/*
		 * Insert work. Count in, that we already sorted experiences so first
		 * and last experiences are known. As we are iterating from last one, we
		 * are searching for the first element that is last :D after that we can
		 * insert already inserted work ids
		 */
		for (int i = experiences.size() - 1; i >= 0; i--) {
			experience = experiences.get(i);

			/* Is this promotion? */
			if (experience.getFromDate() != null && i > 0) {
				/* We are looking for first job with lower level */
				int levelBefore = experiences.get(i - 1).getPositionLevel();
				int index = i - 1;

				if (levelBefore < experience.getPositionLevel()) {
					promoted = true;

					if (i > 1) {
						while (index > 0 && levelBefore == experiences.get(index - 1).getPositionLevel()) {
							index = index - 1;
						}
					}

					daysNeeded = daysBetween(experiences.get(index).getFromDate(), experience.getFromDate());
				}
			}

			if (experience.isLast()) {
				alreadyLast = true;
			}

			companyGeneratedId = insertCompany(connection, escapeSpecialChars(experience.getCompanyName()));
			groupGeneratedId = insertGroup(connection, experience.getPositionGroup(), experience.getPositionLevel());
			positionGeneratedId = insertPosition(connection, groupGeneratedId,
					escapeSpecialChars(experience.getPositionTitle()));

			insertedWorkId = insertWork(connection, graduateGeneratedId, positionGeneratedId, companyGeneratedId,
					fkWorkNext, experience.getFromDateString(), experience.getToDateString(),
					experience.getCurrDateString(), experience.isCurr(), experience.isFirst(), experience.isLast(),
					promoted, daysNeeded);

			if (alreadyLast) {
				fkWorkNext = insertedWorkId;
			}

			daysNeeded = -1;
			promoted = false;

		}

	}

	/* helper methods for inserting into database */

	/**
	 * Creates an sql insert statement and executes it. Altering Graduate table.
	 * Id is auto incremented. Generated key is returned.
	 * 
	 * @param connection
	 * @param name
	 *            - name of graduate
	 * @param surname
	 *            - surname of graduate
	 * @param fkDegree
	 *            - foreign key of bachelor degree
	 */
	private int insertGraduate(Connection connection, String name, String surname, int fkDegree) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int graduateId = -1;

		try {

			if (fkDegree < 0) {
				SQLException e = new SQLDataException("fkDegree (" + fkDegree
						+ ") is less than -1, something wrong while  fetching degree id from eprints. ");
				throw e;
			}

			/* prepare insert */
			preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_GRADUATE
					+ " (name, surname, fkDegree) values (?,?,?)", Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, name);
			preparedStatement.setString(2, surname);
			preparedStatement.setInt(3, fkDegree);

			/* Insert */
			int rownum = preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (rownum != 0 && resultSet.next()) {
				graduateId = resultSet.getInt(1);
			} else {
				throw new SQLException();
			}

			return graduateId;

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	/**
	 * Creates an sql insert statement and executes it. Altering Mentor table
	 * 
	 * @param connection
	 * @param id
	 * @param name
	 * @param surname
	 * 
	 */
	private int insertMentor(Connection connection, String name, String surname) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int mentorId = -1;

		try {

			/*
			 * First there check if mentor with this name and surname is already
			 * in database
			 */
			String whereClause = "where name ='" + name + "' and surname ='" + surname + "'";
			mentorId = getIdOfDuplicate(connection, T_MENTOR, whereClause);

			/*
			 * If -1 returned this mentor does not exists in database. Insert
			 * data.
			 */
			if (mentorId == -1) {

				preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_MENTOR
						+ " (name, surname) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, name);
				preparedStatement.setString(2, surname);

				/* Insert */
				int rownum = preparedStatement.executeUpdate();

				resultSet = preparedStatement.getGeneratedKeys();
				if (rownum != 0 && resultSet.next()) {
					mentorId = resultSet.getInt(1);
				} else {
					throw new SQLException();
				}

			}

			return mentorId;

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Creates an sql insert statement and executes it. Altering
	 * BachelorDegree_has_Keyword table
	 * 
	 * @param connection
	 * @param idDegree
	 * @param idKeyword
	 */
	private void insertDegreeKeyword(Connection connection, int idDegree, int idKeyword) {
		PreparedStatement preparedStatement = null;

		try {

			if (idKeyword < 0 || idDegree < 0) {
				SQLException e = new SQLDataException(
						"idKeyword ("
								+ idKeyword
								+ ") or idDegree ("
								+ idDegree
								+ ") is less than -1, something wrong while inserting keyword or fetching degree id from eprints. ");
				throw e;
			}

			preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_DEGREE_KEYWORD
					+ " values (?, ?)");
			preparedStatement.setInt(1, idDegree);
			preparedStatement.setInt(2, idKeyword);
			/* Insert */
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 * Creates an sql insert statement and executes it. Altering BachelorDegree
	 * table.
	 * 
	 * @param id
	 * @param titleSlo
	 * @param titleAng
	 * @param oralTime
	 * @param fkMentor
	 */
	private void insertDegree(Connection connection, int degreeId, String titleSlo, String titleAng, String oralTime,
			int fkMentor) {

		PreparedStatement preparedStatement = null;

		try {

			if (degreeId < 0) {
				SQLException e = new SQLDataException("degreeId (" + degreeId
						+ ") is less than -1, something wrong while fetching data from eprints");
				throw e;
			}

			preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_DEGREE
					+ " values (?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, degreeId);
			preparedStatement.setString(2, titleSlo);
			preparedStatement.setString(3, titleAng);
			preparedStatement.setString(4, oralTime);
			preparedStatement.setInt(5, fkMentor);

			/* Insert */
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

	}

	/**
	 * Creates an sql insert statement and executes it. Altering Keyword table.
	 * Auto increment ID. Returns generated keyword id.
	 * 
	 * @param keyword
	 */
	private int insertKeyword(Connection connection, String keyword) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int keywordId = -1;

		try {
			/*
			 * First there check if keyword is already in database - keyword is
			 * unique column
			 */
			String whereClause = "where keyword = '" + keyword + "'";
			keywordId = getIdOfDuplicate(connection, T_KEYWORD, whereClause);

			/*
			 * No need to insert if id equals -1, duplicate exists. If id equals
			 * -1, no duplicate is found, so we have to insert it
			 */
			if (keywordId == -1) {
				/* insert */
				preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_KEYWORD
						+ " (keyword) values (?)", Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, keyword);

				/* Insert */
				int rownum = preparedStatement.executeUpdate();

				resultSet = preparedStatement.getGeneratedKeys();
				if (rownum != 0 && resultSet.next()) {
					keywordId = resultSet.getInt(1);
				} else {
					throw new SQLException();
				}
			}

			return keywordId;

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Creates an sql insert statement and executes it. Altering Position table
	 * 
	 * @param title
	 */
	private int insertPosition(Connection connection, int fkWorkgroup, String title) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int positionId = -1;

		try {

			/*
			 * First there check if this title is already in database - title is
			 * unique column
			 */
			String whereClause = "where title = '" + title + "'";
			positionId = getIdOfDuplicate(connection, T_POSITION, whereClause);

			/*
			 * No need to insert if id equals -1, duplicate exists. If id equals
			 * -1, no duplicate is found, so we have to insert it
			 */
			if (positionId == -1) {

				/* insert */
				preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_POSITION
						+ " (fkWorkgroup, title) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setInt(1, fkWorkgroup);
				preparedStatement.setString(2, title);

				/* Insert */
				int rownum = preparedStatement.executeUpdate();

				resultSet = preparedStatement.getGeneratedKeys();
				if (rownum != 0 && resultSet.next()) {
					positionId = resultSet.getInt(1);
				} else {
					throw new SQLException();
				}

			}

			return positionId;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Creates an sql insert statement and executes it. Altering Keyword table
	 * 
	 * @param Id
	 * @param name
	 */
	private int insertCompany(Connection connection, String name) {

		/*
		 * if name is null, then sqlexception will be thrown as column name
		 * cannot be null
		 */
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int companyId = -1;

		try {

			/*
			 * First there check if this title is already in database - title is
			 * unique column.
			 */
			String whereClause = "where name = '" + name + "'";
			companyId = getIdOfDuplicate(connection, T_COMPANY, whereClause);

			/*
			 * No need to insert if id equals -1, duplicate exists. If id equals
			 * -1, no duplicate is found, so we have to insert it
			 */
			if (companyId == -1) {

				/* insert */
				preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_COMPANY
						+ " (name) values (?)", Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, name);

				/* Insert */
				int rownum = preparedStatement.executeUpdate();

				resultSet = preparedStatement.getGeneratedKeys();
				if (rownum != 0 && resultSet.next()) {
					companyId = resultSet.getInt(1);
				} else {
					throw new SQLException();
				}

			}

			return companyId;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Creates an sql insert statement and executes it. Altering Group table
	 * 
	 * @param Id
	 * @param name
	 * @param level
	 */
	private int insertGroup(Connection connection, String name, int level) {

		/*
		 * if name is null, then sqlexception will be thrown as column name
		 * cannot be null
		 */
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int groupId = -1;

		try {

			/*
			 * First there check if this title is already in database - title is
			 * unique column.
			 */
			String whereClause = "where name = '" + name + "'";
			System.out.println(whereClause);
			groupId = getIdOfDuplicate(connection, T_WORKGROUP, whereClause);

			/*
			 * No need to insert if id equals -1, duplicate exists. If id equals
			 * -1, no duplicate is found, so we have to insert it
			 */
			if (groupId == -1) {

				/* insert */
				preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_WORKGROUP
						+ " (name, level) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, name);
				preparedStatement.setInt(2, level);

				/* Insert */
				int rownum = preparedStatement.executeUpdate();

				resultSet = preparedStatement.getGeneratedKeys();
				if (rownum != 0 && resultSet.next()) {
					groupId = resultSet.getInt(1);
				} else {
					throw new SQLException();
				}

			}

			return groupId;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * 
	 * Creates an sql insert statement and executes it. Altering Work table.
	 * 
	 * @param connection
	 * @param Id
	 * @param fkGraduate
	 * @param fkPosition
	 * @param fkCompany
	 * @param fromDate
	 * @param toDate
	 * @param currDate
	 * @param temp
	 */
	private int insertWork(Connection connection, int fkGraduate, int fkPosition, int fkCompany, int fkWorkNext,
			String fromDate, String toDate, String currDate, boolean curr, boolean first, boolean last,
			boolean promoted, int daysNeeded) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int workId = -1;

		try {

			if (fkPosition < 0 && fkCompany < 0) {
				SQLException e = new SQLDataException("fkPosition (" + fkPosition + ") or fkCompany (" + fkCompany
						+ ") is less than -1, probably no data about position and company");
				throw e;
			}

			preparedStatement = connection.prepareStatement("insert into careeranalysist." + T_WORK
					+ " (fkGraduate, fkPosition, fkCompany, fkWorkNext,fromDate, toDate, currDate,"
					+ " curr, first, last, promoted, daysNeeded) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setInt(1, fkGraduate);
			preparedStatement.setInt(2, fkPosition);
			preparedStatement.setInt(3, fkCompany);
			if (fkWorkNext < 0) {
				preparedStatement.setNull(4, java.sql.Types.INTEGER);
			} else {
				preparedStatement.setInt(4, fkWorkNext);
			}
			preparedStatement.setString(5, fromDate);
			preparedStatement.setString(6, toDate);
			preparedStatement.setString(7, currDate);
			preparedStatement.setBoolean(8, curr);
			preparedStatement.setBoolean(9, first);
			preparedStatement.setBoolean(10, last);
			preparedStatement.setBoolean(11, promoted);
			preparedStatement.setInt(12, daysNeeded);

			/* Insert */
			int rownum = preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (rownum != 0 && resultSet.next()) {
				workId = resultSet.getInt(1);
			} else {
				throw new SQLException();
			}

			return workId;

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	/**
	 * This method executes query on specified table (tableName) with specified
	 * where clause (whereClause). If row exists, id of this row is returned,
	 * otherwise -1. If more then one row is retrieved id of first is returned.
	 * 
	 * @param connection
	 * @param tableName
	 * @param whereClause
	 * @return - id of first row. If no row exists, -1 is returned.
	 * @throws SQLException
	 */
	private int getIdOfDuplicate(Connection connection, String tableName, String whereClause) {

		Statement statement = null;
		ResultSet resultSet = null;
		try {

			if (whereClause == null || whereClause.isEmpty()) {
				throw new SQLDataException();
			}
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM " + tableName + " " + whereClause);

			int idOfDuplicate = -1;
			if (resultSet != null && resultSet.first()) {
				idOfDuplicate = resultSet.getInt(1);
			}

			return idOfDuplicate;

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	private static ArrayList<LinkedInExperienceModel> sort(ArrayList<LinkedInExperienceModel> experiences) {

		/* Create comparator on the fly and sort experiences by date */
		Collections.sort(experiences, new Comparator<LinkedInExperienceModel>() {
			public int compare(LinkedInExperienceModel o1, LinkedInExperienceModel o2) {
				if (o1.getFromDate() == null || o2.getFromDate() == null)
					return 0;
				return o1.getFromDate().compareTo(o2.getFromDate());
			}
		});

		/*
		 * Experiences are now sorted. But those with null fromDate should not
		 * be counted in carrer path! They are positioned as elements in list
		 * but they are not last jobs. Now, set first and last!
		 */
		for (int i = 0; i < experiences.size(); i++) {
			if (experiences.get(i).getFromDate() != null) {
				/* set as first and break from loop */
				experiences.get(i).setFirst(true);
				break;
			}
		}

		for (int i = experiences.size() - 1; i >= 0; i--) {
			if (experiences.get(i).getFromDate() != null) {
				/* set as last and break from loop */
				experiences.get(i).setLast(true);
				break;
			}
		}

		return experiences;

	}

	/**
	 * Escaping special characters. Actually adding one more \ to special
	 * character so that they are escaped in mysql queries.
	 * 
	 * @param input
	 * @return
	 */
	private String escapeSpecialChars(String input) {

		final StringBuilder result = new StringBuilder();
		final StringCharacterIterator iterator = new StringCharacterIterator(input);
		char character = iterator.current();
		while (character != CharacterIterator.DONE) {
			if (character == '\'') {
				result.append("\\'");
			} else if (character == '\"') {
				result.append("\\\"");
			} else if (character == '\\') {
				result.append("\\\\");
			} else {
				/* Char is not special. Add it to result */
				result.append(character);
			}
			character = iterator.next();
		}

		return result.toString();
	}

	public static int daysBetween(Date startDate, Date endDate) {

		if (startDate == null || endDate == null) {
			return -1;
		}

		Calendar start = Calendar.getInstance();
		start.setTime(startDate);

		Calendar end = Calendar.getInstance();
		end.setTime(endDate);

		Calendar startClone = (Calendar) start.clone();
		int daysBetween = 0;
		while (startClone.before(end)) {
			startClone.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}

		return daysBetween;
	}

	/**
	 * This method closes connection to database.
	 * 
	 * @param connection
	 */
	public void close(Connection connection) {
		try {

			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
