package com.igorv.careerAnalysis.linkedIn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LinkedInExperienceModel {

	private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private String companyName;
	private String positionTitle;
	private String fromDateString;
	private String toDateString;
	private String currDateString;
	private String positionGroup;
	private int positionLevel;

	/*
	 * You might not have job right now but you may had last job, that is not
	 * your current job
	 */
	private boolean curr = false;
	private boolean first = false;
	private boolean last = false;

	public LinkedInExperienceModel(String companyName, String positionTitle, String fromDate, String toDate,
			String currDate, String positionGroup, int positionLevel) {
		this.companyName = companyName;
		this.positionTitle = positionTitle;
		this.fromDateString = fromDate;
		this.toDateString = toDate;
		this.currDateString = currDate;
		this.positionGroup = positionGroup;
		this.positionLevel = positionLevel;

		if (toDate == null && currDate != null) {
			curr = true;

		} else {
			curr = false;
		}

		repairDates();
	}

	private void repairDates() {
		String concat = "-01-01";

		if (fromDateString != null && fromDateString.length() == 4) {
			fromDateString = fromDateString + concat;
		}

		if (toDateString != null && toDateString.length() == 4) {
			toDateString = toDateString + concat;
		}

		if (currDateString != null && currDateString.length() == 4) {
			currDateString = currDateString + concat;
		}
	}

	public boolean isLast() {
		return last;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public String getFromDateString() {
		return fromDateString;
	}

	public String getToDateString() {
		return toDateString;
	}

	public String getCurrDateString() {
		return currDateString;
	}

	public Date getFromDate() {

		if (getFromDateString() == null) {
			return null;
		}

		try {
			return DATE_FORMAT.parse(fromDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Date getToDate() {

		if (getToDateString() == null) {
			return null;
		}

		try {
			return DATE_FORMAT.parse(toDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Date getCurrDate() {

		if (getCurrDateString() == null) {
			return null;
		}

		try {
			return DATE_FORMAT.parse(currDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isCurr() {
		return curr;
	}

	public void setCurr(boolean curr) {
		this.curr = curr;
	}

	public String getPositionGroup() {
		return positionGroup;
	}

	public int getPositionLevel() {
		return positionLevel;
	}

	@Override
	public String toString() {

		return "Position: " + getPositionTitle() + " ; Company: " + getCompanyName() + " ; From: " + getFromDate()
				+ "; ToDate: " + getToDate() + " ; CurrDate: " + getCurrDate();
	}
}
