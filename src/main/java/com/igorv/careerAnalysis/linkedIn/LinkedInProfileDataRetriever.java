package com.igorv.careerAnalysis.linkedIn;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.igorv.careerAnalysis.database.TitleHierarchy;

/**
 * 
 * @author Igor
 * 
 *         This class retrieves neccessary data from profile. Profile is
 *         actually linkedIn profile html page converted to jsoup.Document
 *         class.
 */
public class LinkedInProfileDataRetriever {

	private static String PROFILE_EXPERIENCE_ID = "profile-experience";

	public static ArrayList<LinkedInExperienceModel> constructAndGetExperiences(Document profileDoc) {

		ArrayList<LinkedInExperienceModel> experiences = new ArrayList<LinkedInExperienceModel>();

		String companyName = null;
		String positionTitle = null;

		/* Get experience section */
		Element experienceSection = profileDoc.getElementById(PROFILE_EXPERIENCE_ID);

		if (experienceSection != null) {

			/* Get positions */
			Elements positionsElements = experienceSection.getElementsByClass("position");

			if (positionsElements != null && !positionsElements.isEmpty()) {

				/* Iterate through all position elements */
				for (Element position : positionsElements) {
					/*
					 * Get position title - we are interested only in positions
					 * with position title.
					 */
					Element postitle = position.getElementsByClass("postitle").first();

					if (postitle == null) {
						continue;
					}
					/*
					 * Position title text is inside first postitle/h3 element
					 */
					positionTitle = postitle.getElementsByTag("h3").first().text();

					/*
					 * Company name text is inside first postitle/h4 element
					 */
					companyName = postitle.getElementsByTag("h4").first().text();

					if (positionTitle == null && companyName == null) {
						continue;
					}

					String[] dates = getDates(position);
					String[] titleHierarchy = TitleHierarchy.getHierarchyGroupAndLevel(positionTitle);

					/*
					 * Create experience model that will contain data about one
					 * experience item
					 */
					experiences.add(new LinkedInExperienceModel(companyName, positionTitle, dates[0], dates[1],
							dates[2], titleHierarchy[0], TitleHierarchy.getHierarchyLevel(titleHierarchy[1])));

				}

				return experiences;
			}
		}

		return null;
	}

	/**
	 * Returns dates in string array. First element is start date, second is end
	 * date and third is currDate
	 * 
	 * @param period
	 * @return
	 */
	private static String[] getDates(Element position) {

		String fromDate = null;
		String toDate = null;
		String currDate = null;

		/* Get period element */
		Element period = position.getElementsByClass("period").first();

		/* Get start date */
		Element dtstart = period.getElementsByClass("dtstart").first();

		if (dtstart != null) {
			fromDate = dtstart.attr("title");
		}

		/* Get end date */
		Element dtend = period.getElementsByClass("dtend").first();

		if (dtend != null) {
			toDate = dtend.attr("title");

		}

		/* Current date */
		Element dtstamp = period.getElementsByClass("dtstamp").first();

		if (dtstamp != null) {
			currDate = dtstamp.attr("title");
		}

		String[] dates = { fromDate, toDate, currDate };
		return dates;

	}
}
