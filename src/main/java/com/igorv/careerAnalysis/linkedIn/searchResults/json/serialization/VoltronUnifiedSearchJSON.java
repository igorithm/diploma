package com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization;

public class VoltronUnifiedSearchJSON {

	private Search search;

	@Override
	public String toString() {
		return "	VoltronUnifiedSearchJSON:{ \n 	" + search.toString() + "}";
	}

	public Search getSearch() {
		return search;
	}
	
	
}
