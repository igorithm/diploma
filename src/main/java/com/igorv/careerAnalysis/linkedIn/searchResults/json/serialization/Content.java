package com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization;

public class Content {

	private VoltronUnifiedSearchJSON voltron_unified_search_json;

	@Override
	public String toString() {
		return "	Content:{ \n	" + voltron_unified_search_json.toString() + "}";
	}

	public VoltronUnifiedSearchJSON getVoltron_unified_search_json() {
		return voltron_unified_search_json;
	}

}
