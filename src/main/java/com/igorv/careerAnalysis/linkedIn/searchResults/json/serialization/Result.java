package com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization;

public class Result {

	private Person person;

	@Override
	public String toString() {
		if (isPerson()) {
			return "\n	Result:{ \n	" + person.toString() + "}";
		} else {
			return "\n	Result:{ \n	" + null + "}";
		}
	}

	public boolean isPerson() {
		if (this.person != null) {
			return true;
		} else {
			return false;
		}
	}

	public Person getPerson() {
		return person;
	}
	
	
}
