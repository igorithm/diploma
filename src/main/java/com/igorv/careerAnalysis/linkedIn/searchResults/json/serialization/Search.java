package com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization;

import java.util.ArrayList;

public class Search {

	private String results_count_with_keywords_i18n;
	private ArrayList<Result> results;

	@Override
	public String toString() {
		return "	Search:{ \n 	" + "results_count_with_keywords_i18 = " + results_count_with_keywords_i18n + ", "
				+ results.toString() + "}";
	}

	public String getResults_count_with_keywords_i18n() {
		return results_count_with_keywords_i18n;
	}

	public ArrayList<Result> getResults() {
		return results;
	}
	
	
}
