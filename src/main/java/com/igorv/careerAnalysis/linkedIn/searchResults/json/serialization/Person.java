package com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization;

public class Person {

	private String lastName;
	private String firstName;
	private String link_nprofile_view_3;
	private String link_nprofile_view_4;

	@Override
	public String toString() {
		return "	Person:{ \n 	" + "lastName = " + lastName + ", firstName = " + firstName + ", link_3 = "
				+ link_nprofile_view_3 + ", link_4 = " + link_nprofile_view_4 + "}";
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLink_nprofile_view_3() {
		return link_nprofile_view_3;
	}

	public String getLink_nprofile_view_4() {
		return link_nprofile_view_4;
	}
	
}
