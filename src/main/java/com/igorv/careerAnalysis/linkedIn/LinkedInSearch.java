package com.igorv.careerAnalysis.linkedIn;

import java.io.IOException;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.google.gson.Gson;
import com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization.MainJSON;

public class LinkedInSearch {

	private LinkedInManager parent;

	/* HTML element values */
	private static final String SEARCH_FORM_NAME = "commonSearch";
	private static final String SEARCH_BUTTON_NAME = "search";
	private static final String SEARCH_FIELD = "keywords";

	public LinkedInSearch(LinkedInManager parent) {
		this.parent = parent;
	}

	/**
	 * This method performs search by passed parameter searchQuery on passed
	 * html page 'page'.
	 * 
	 * @param searchQuery
	 * @param page
	 * @return
	 */
	public MainJSON searchPeople(String searchQuery, HtmlPage page) {

		System.out.println("Search query = " + searchQuery);

		try {

			/* Get the form from page */
			final HtmlForm form = (HtmlForm) page.getFormByName(SEARCH_FORM_NAME);
			final HtmlTextInput searchField = (HtmlTextInput) form.getInputByName(SEARCH_FIELD);

			/* Fill the form */
			searchField.setValueAttribute(searchQuery);

			/*
			 * Now submit the form by clicking the button and get back the
			 * second page, which is our page with search results
			 */
			HtmlPage searchResultsPage = (HtmlPage) form.getButtonByName(SEARCH_BUTTON_NAME).click();

			MainJSON mainJson = deserializeJSON(searchResultsPage);

			return mainJson;

		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
		} catch (ElementNotFoundException e) {
			System.err.println("Could not find right element.");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * This method accepts HtmlPage object, finds json, deserializes it and
	 * returns json java object.
	 * 
	 * @param searchResultsPage
	 * @return MainJSON object - deserialized json
	 */
	private static MainJSON deserializeJSON(HtmlPage searchResultsPage) {
		Gson gson = new Gson();
		/* Get JSON object */
		MainJSON mainJson = gson.fromJson(getCommentedJsonResults(searchResultsPage), MainJSON.class);
		return mainJson;
	}

	/**
	 * 
	 * LinkedIn is returning results in JSON format. Inside code element comment
	 * with json content is located. This method accepts htmlPage, finds this
	 * comment, retrieves it and returns JSON (not yet deserialized) as string.
	 * This is helper method for deserializeJSON method and is not intended to
	 * be used as a stand-alone unit.
	 * 
	 * @param searchResultsPage
	 * @return
	 */
	public static String getCommentedJsonResults(HtmlPage searchResultsPage) {

		/* Get page content as String */
		String searchResultsPageContent = searchResultsPage.getWebResponse().getContentAsString();

		/* On results page there is only one code tag */
		int indexOfCode = searchResultsPageContent.indexOf("<code");

		/*
		 * Search for comment inside code tag. XML comment starts with <!-- and
		 * ends with -->. Add 4 to start index in order to retrieve content
		 * without starting comment tag
		 */
		int indexOfStart = searchResultsPageContent.indexOf("<!--", indexOfCode) + 4;
		int indexOfEnd = searchResultsPageContent.indexOf("-->", indexOfCode);

		String jsonContent = searchResultsPageContent.substring(indexOfStart, indexOfEnd);

		/* Remove distance property as this is causing errors */
		jsonContent = jsonContent.replaceAll(":&dsh;1", ":\"&dsh;1\"").replaceAll(":\\\\u002d1,", ":\"test\",");

		return jsonContent;
	}

	public void setParent(LinkedInManager parent) {
		this.parent = parent;
	}

	public LinkedInManager getParent() {
		return parent;
	}

}
