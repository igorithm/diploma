package com.igorv.careerAnalysis.linkedIn;

import java.util.ArrayList;

import org.jsoup.nodes.Document;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.igorv.careerAnalysis.eprints.serialization.Eprint;
import com.igorv.careerAnalysis.eprints.serialization.Eprints;
import com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization.MainJSON;

/**
 * 
 * @author Igor Vinojcic
 * 
 */
public class LinkedInManager {

	// private LinkedInProfileValidatior profileValidator;
	private LinkedInSearch searchManager;
	private LinkedInLogin loginManager;
	private LinkedInProfileValidator profileValidator;

	private WebClient browser;
	private HtmlPage mainPage = null;
	private Eprints eprints;

	public LinkedInManager(Eprints eprints) {

		this.eprints = eprints;

		this.browser = new WebClient();
		/*
		 * Set webClient options. LinkedIn has some javascript issues so we will
		 * disable javascript. There are some issues with CSS so CSS is disabled
		 * also.
		 */
		this.browser.getOptions().setJavaScriptEnabled(false);
		this.browser.getOptions().setCssEnabled(false);

		this.loginManager = new LinkedInLogin(this);
		this.searchManager = new LinkedInSearch(this);
		this.profileValidator = new LinkedInProfileValidator(this);

	}

	/**
	 * Executes search on main linkedIn page. Retrieves search results and looks
	 * for valid profile. If valid profile is found, data we are interested in
	 * is retrieved.
	 * 
	 * @param eprint
	 * @return Returns ArrayList containing LinkedInExperienceModel objects,
	 *         which are containing data we are interested in. If null returned
	 *         no valid profile found or no data found.
	 */
	public ArrayList<LinkedInExperienceModel> getValidProfileData(Eprint eprint) {

		try {

			if (this.mainPage == null) {
				/* Login and get mainPage */
				this.mainPage = loginManager.login();
			}

			/* Construct search query - Given name + Family name */
			String searchQuery = eprint.getCreators().getItems().get(0).getName().getGiven() + " "
					+ eprint.getCreators().getItems().get(0).getName().getFamily();

			/*
			 * get search JSON results
			 */
			MainJSON linkedInSearchResultsJSON = searchManager.searchPeople(searchQuery, this.mainPage);

			/**
			 * JSON results should be checked/validated if there is a right
			 * profile.
			 */
			Document validProfile = profileValidator.getValidProfile(eprint, linkedInSearchResultsJSON);

			if (validProfile == null) {
				return null;
			}

			/* Retrieve data */

			return LinkedInProfileDataRetriever.constructAndGetExperiences(validProfile);

		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
		}

		return null;

	}

	public WebClient getBrowser() {
		return browser;
	}

	public void setBrowser(WebClient browser) {
		this.browser = browser;
	}

	public Eprints getEprints() {
		return eprints;
	}

	public void setEprints(Eprints eprints) {
		this.eprints = eprints;
	}

}
