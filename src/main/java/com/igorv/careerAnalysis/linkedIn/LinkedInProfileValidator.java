package com.igorv.careerAnalysis.linkedIn;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.igorv.careerAnalysis.eprints.serialization.Eprint;
import com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization.MainJSON;
import com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization.Person;
import com.igorv.careerAnalysis.linkedIn.searchResults.json.serialization.Result;

public class LinkedInProfileValidator {

	private LinkedInManager parent;

	private static String LINKEDIN_ULR = "http://www.linkedin.com";

	/*
	 * Faculty validation keywords - some possibilites: Computer Science and
	 * Mathematics, Faculty for computer and information science, Ljubljana
	 * Faculty of Coumputer and Information Science, Faculty of Computer and
	 * Information Science
	 */
	private static final String EDUCATION_COMPUTER_ANG = "computer";
	private static final String EDUCATION_INFORMATION_ANG = "information";
	private static final String EDUCATION_COMPUTER_SLO = "racunalnistv";
	private static final String EDUCATION_INFORMATION_SLO_1 = "informatik";
	private static final String EDUCATION_INFORMATION_SLO_2 = "informacij";
	private static final String EDUCATION_ENGINEERS_DEGREE = "engineer's degree, cs";
	private static final String EDUCATION_ENGINEERING = "engineering";
	private static final String EDUCATION_SCIENCE = "science";
	private static final String EDUCATION_TECHNOLOGY_ANG = "technology";
	private static final String EDUCATION_TECHNOLOGY_SLO = "tehnolog";
	private static final String EDUCATION_FRI = "fri";

	/* parsing static variables - helpers */
	private static final String PROFILE_EDUCATION_ID = "profile-education";
	private static final HashMap<Character, Character> caronSubs;
	static {
		caronSubs = new HashMap<Character, Character>();
		caronSubs.put('č', 'c');
		caronSubs.put('Č', 'C');
		caronSubs.put('ć', 'c');
		caronSubs.put('Ć', 'C');
		caronSubs.put('š', 's');
		caronSubs.put('Š', 'S');
		caronSubs.put('ž', 'z');
		caronSubs.put('Ž', 'Z');
	}

	/* Constructor */
	public LinkedInProfileValidator(LinkedInManager parent) {
		this.parent = parent;
	}

	/**
	 * Takes search results in JSON format. Iterates through all results and
	 * finds one profile that belongs to the specified graduate. This method
	 * uses specified conditions to validate profle. If right profile is found,
	 * jsoup document of profile page is returned. It is easier to parse with
	 * Jsoup library then HtmlUnit library.
	 * 
	 * @param linkedInResults
	 * @return
	 */
	public Document getValidProfile(Eprint eprint, MainJSON linkedInJSON) {

		ArrayList<Result> results;
		HtmlPage profilePage;
		Document profileDoc;

		try {
			/* Get results from JSON */
			results = linkedInJSON.getContent().getVoltron_unified_search_json().getSearch().getResults();

			for (Result result : results) {

				if (result != null && result.isPerson()) {
					/* Remove amp; from url */
					String url = result.getPerson().getLink_nprofile_view_3().replaceAll("amp;", "");

					/* Get profile url */
					URL profileURL = new URL(LINKEDIN_ULR + url);

					if (profileURL != null) {

						/*
						 * Get profile page and convert it to jsoup document
						 * class...
						 */
						profilePage = (HtmlPage) parent.getBrowser().getPage(profileURL);
						profileDoc = Jsoup.parse(profilePage.getWebResponse().getContentAsString());

						/* Validate */
						if (validateName(eprint, result.getPerson())) {
							if (validateFacultyAndGraduationDate(eprint, profileDoc)) {
								return profileDoc;
							}

						}
					}
				}

			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/* Validators */

	/**
	 * This method validates name. If name and family name of eprint and
	 * linkedIn equals, true is returned. This method compares strings without
	 * carons
	 * 
	 * @param eprint
	 * @param linkedInJSONPerson
	 * @return
	 */
	private boolean validateName(Eprint eprint, Person linkedInJSONPerson) {

		/* Eprint data */
		String eprintGiven = replaceCaron(eprint.getCreators().getItems().get(0).getName().getGiven());
		String eprintFamily = replaceCaron(eprint.getCreators().getItems().get(0).getName().getFamily());

		/* LinkedIn data */
		String linkedInGiven = replaceCaron(linkedInJSONPerson.getFirstName());
		String linkedInFamily = replaceCaron(linkedInJSONPerson.getLastName());

		System.out.println("LinkedIn Name = " + linkedInGiven + " " + linkedInFamily);
		System.out.println("Eprint Name = " + eprintGiven + " " + eprintFamily);

		if (eprintGiven.equalsIgnoreCase(linkedInGiven)) {
			if (eprintFamily.equalsIgnoreCase(linkedInFamily)) {
				System.out.println("Name validation successful!!");
				return true;
			}
		}

		System.out.println("Name validation NOT successful!!");

		return false;
	}

	/**
	 * Validates faculty and graduation date
	 * 
	 * @param eprint
	 * @param profileDoc
	 * @return
	 */
	private boolean validateFacultyAndGraduationDate(Eprint eprint, Document profileDoc) {

		/* Iterate through profile-education id tag */
		Element profile_education = profileDoc.getElementById(PROFILE_EDUCATION_ID);

		if (profile_education == null) {
			return false;
		}

		Elements positions = profile_education.getElementsByClass("position");

		if (positions == null || positions.isEmpty()) {
			return false;
		}

		for (Element position : positions) {
			/* Inside first h3 element is the title of educational organization */
			String educationalInstitution = position.getElementsByTag("h3").first().text();
			String educationalInstitutionDetails = position.getElementsByTag("h4").first().text();
			educationalInstitution = educationalInstitution.toLowerCase();

			System.out.println("Educational Institution: " + educationalInstitution);
			System.out.println("Educational Institution details: " + educationalInstitutionDetails);

			/*
			 * Validate faculty name
			 */
			if (!validateFacultyName(educationalInstitution + " " + educationalInstitutionDetails)) {
				continue;
			}

			/**
			 * Validate graduation date. Take oral date from eprints document
			 * (only Year) and compare it with end of education from linkedIn.
			 */
			if (!validateGraduationYear(eprint, position)) {
				continue;
			}

			return true;
		}

		return false;
	}

	/* Helper Methods */

	/**
	 * Validates faculty name
	 * 
	 * @param educationalInstitution
	 * @return
	 */
	private boolean validateFacultyName(String institution) {

		/*
		 * Prepare institution string - replace chars with carons and make it
		 * lower case
		 */
		institution = replaceCaron(institution.toLowerCase());

		System.out.println("String to validate: " + institution);

		/*
		 * TODO TREBA ŠE RAZMISLIT ČE SAMO PIŠE UNIVERSITY OF LJUBLJANA IN JE
		 * LETNICA PRAVA: POVEZAVA Z MENTORJEM ALI UKVARJANJE Z INFORMATIKO,
		 * RAČUNALNIŠTVO PROGRAMIRANJEM..
		 */

		/*
		 * Name must contain one of conditions: "computer and science" or
		 * "informaton and science" or "engineer's degree, cs" or "fri" or
		 * "ul fri" or "računalništv" or "informatik" or
		 * "computer and enginnering" or "computer and mathematics" or
		 * "računalništv and matematik"
		 */

		/* Optimization */
		boolean isScience = institution.contains(EDUCATION_SCIENCE);

		/* computer and science */
		if (institution.contains(EDUCATION_COMPUTER_ANG) && isScience) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* informaton and science */
		if (institution.contains(EDUCATION_INFORMATION_ANG)
				&& (isScience || institution.contains(EDUCATION_TECHNOLOGY_ANG))) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* racunalnistv */
		if (institution.contains(EDUCATION_COMPUTER_SLO)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* informatik */
		if (institution.contains(EDUCATION_INFORMATION_SLO_1)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* informacij tehnolog */
		if (institution.contains(EDUCATION_INFORMATION_SLO_2) && institution.contains(EDUCATION_TECHNOLOGY_SLO)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* engineer's degree, cs */
		if (institution.contains(EDUCATION_ENGINEERS_DEGREE)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* computer and science */
		if (institution.contains(EDUCATION_COMPUTER_ANG) && institution.contains(EDUCATION_ENGINEERING)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		/* fri */
		if (institution.contains(EDUCATION_FRI)) {
			System.out.println("Faculty name is VALID");
			return true;
		}

		System.out.println("Faculty validation NOT succesfull!!");
		return false;
	}

	/**
	 * Validates graduation year for specific eprint and position
	 * 
	 * @param eprint
	 * @param position
	 * @return
	 */
	private boolean validateGraduationYear(Eprint eprint, Element position) {

		/* Get year end year from linkedIn profile */
		Elements periods = position.getElementsByClass("period");
		if (periods == null || periods.isEmpty()) {
			return false;
		}
		Elements dtends = periods.first().getElementsByClass("dtend");
		if (dtends == null || dtends.isEmpty()) {
			return false;
		}

		String linkedInGraduationYear = dtends.first().text();
		String eprintGraduationYear = null;
		String eprintGraduationYearMinusOne = null;

		if (eprint.getOralDateYear() == null || eprint.getOralDateYear().isEmpty()) {
			eprintGraduationYear = eprint.getDateYear();
			eprintGraduationYearMinusOne = eprint.getDateYearMinusOne();
		} else {
			eprintGraduationYear = eprint.getOralDateYear();
			eprintGraduationYearMinusOne = eprint.getOralDateYearMinusOne();
		}

		/* no year not valid */
		if (eprintGraduationYear == null) {
			return false;
		}

		System.out.println("Eprint Year: " + eprintGraduationYear);
		System.out.println("Eprint Year -1: " + eprintGraduationYearMinusOne);
		System.out.println("LinkedIn Year: " + linkedInGraduationYear);

		if (!eprintGraduationYear.equalsIgnoreCase(linkedInGraduationYear)) {
			System.out.println("Graduation date validation NOT successful!!");
			/* Check also for minus one year */
			if (!(eprintGraduationYearMinusOne.equalsIgnoreCase(linkedInGraduationYear) && checkUniqueness(eprint))) {
				return false;
			}
		}

		System.out.println("Graduation date validation successful!!");
		return true;

	}

	/**
	 * Compares this eprint object (graduate hereinafter) with other graduates
	 * in "eprints list". This method looks for another graduate with same name,
	 * surname and graduation year (graduation year -1 of passed graduate is
	 * taken).
	 * 
	 * @param eprint
	 *            - graduate eprint profile.
	 * @return False if such a student is found.
	 */
	private boolean checkUniqueness(Eprint eprint) {

		for (Eprint eprint_comp : parent.getEprints().getEprints()) {

			/* Do not compare objects with same references */
			if (eprint.equals(eprint_comp)) {
				continue;
			}

			/* eprint data */
			String eprintGiven = eprint.getCreators().getItems().get(0).getName().getGiven();
			String eprintFamily = eprint.getCreators().getItems().get(0).getName().getFamily();

			/* eprint_comp data */
			String eprintCompGiven = eprint_comp.getCreators().getItems().get(0).getName().getGiven();
			String eprintCompFamily = eprint_comp.getCreators().getItems().get(0).getName().getFamily();

			if (eprintGiven.equalsIgnoreCase(eprintCompGiven) && eprintFamily.equalsIgnoreCase(eprintCompFamily)
					&& eprint.getOralDateYearMinusOne().equalsIgnoreCase(eprint_comp.getOralDateYear())) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Replaces all chars with caron in String "toReplace" with associated chars
	 * without carons
	 * 
	 * @param toReplace
	 * @return
	 */
	private static String replaceCaron(String toReplace) {

		for (char c : caronSubs.keySet()) {

			if (toReplace.contains(c + "")) {
				toReplace = toReplace.replace(c, caronSubs.get(c));
			}
		}

		return toReplace;
	}
}
