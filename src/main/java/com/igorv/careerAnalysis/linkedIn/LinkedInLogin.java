package com.igorv.careerAnalysis.linkedIn;

import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class LinkedInLogin {

	private LinkedInManager parent;

	/* User credentials */
	private static final String USER = "igorvinojcic@gmail.com";
	private static final String PASSWORD = "d12m20i77";

	/* URLs */
	public static final String LOGIN_PAGE_URL = "https://www.linkedin.com/uas/login";

	/* HTML element values */
	private static final String LOGIN_BUTTON_NAME = "Sign In";
	private static final String LOGIN_EMAIL_FIELD_NAME = "session_key";
	private static final String LOGIN_PASS_FIELD_NAME = "session_password";

	public LinkedInLogin(LinkedInManager parent) {
		this.parent = parent;
	}

	/**
	 * Performs login action by submitting login form. If login is successful
	 * main page (users home page) is received. Main page can be accessed
	 * through static variable mainPage that is initialized by this method.
	 */
	public HtmlPage login() {
		try {

			HtmlPage loginPage = (HtmlPage) parent.getBrowser().getPage(LinkedInLogin.LOGIN_PAGE_URL);

			/* Get the form */
			final HtmlForm form = (HtmlForm) loginPage.getForms().get(0);
			final HtmlSubmitInput button = (HtmlSubmitInput) form.getInputByValue(LOGIN_BUTTON_NAME);
			final HtmlTextInput emailField = (HtmlTextInput) form.getInputByName(LOGIN_EMAIL_FIELD_NAME);
			final HtmlPasswordInput passField = (HtmlPasswordInput) form.getInputByName(LOGIN_PASS_FIELD_NAME);

			/* Fill the form */
			emailField.setValueAttribute(USER);
			passField.setValueAttribute(PASSWORD);

			/*
			 * Now submit the form by clicking the button and get back the
			 * second page, which is our main page.
			 */
			return (HtmlPage) button.click();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
}
