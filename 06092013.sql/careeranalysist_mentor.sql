CREATE DATABASE  IF NOT EXISTS `careeranalysist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `careeranalysist`;
-- MySQL dump 10.13  Distrib 5.6.12, for Win64 (x86_64)
--
-- Host: localhost    Database: careeranalysist
-- ------------------------------------------------------
-- Server version	5.6.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mentor`
--

DROP TABLE IF EXISTS `mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mentor` (
  `idMentor` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  PRIMARY KEY (`idMentor`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentor`
--

LOCK TABLES `mentor` WRITE;
/*!40000 ALTER TABLE `mentor` DISABLE KEYS */;
INSERT INTO `mentor` VALUES (1,'Janez','Demšar'),(2,'Marko','Robnik Šikonja'),(3,'Borut','Batagelj'),(4,'Tomaž','Dobravec'),(5,'Matjaž Branko','Jurič'),(6,'Dejan','Lavbič'),(7,'Rok','Rupnik'),(8,'Branko','Šter'),(9,'Igor','Kononenko'),(10,'Mira','Trebar'),(11,'Peter','Peer'),(12,'Marko','Bajec'),(13,'Matjaž B.','Jurič'),(14,'Patricio','Bulić'),(15,'Borut','Robič'),(16,'Uroš','Lotrič'),(17,'Emil','Žagar'),(18,'Franc','Solina'),(19,'Andrej','Brodnik'),(20,'Matija','Marolt'),(21,'Viljan','Mahnič'),(22,'Mojca','Ciglarič'),(23,'Danijel','Skočaj'),(24,'Matjaž','Kukar'),(25,'Zoran','Bosnić'),(26,'Blaž','Zupan'),(27,'Ivan','Bratko'),(28,'Luka','Šajn'),(29,'Igor','Rožanc'),(30,'Aljaž','Zrnec'),(31,'Damjan','Vavpotič'),(32,'Dušan','Kodek'),(33,'Marjan','Krisper'),(34,'Miha','Mraz'),(35,'Franc','Jager'),(36,'Boštjan','Slivnik'),(37,'Saša','Divjak'),(38,'Riste','Škrekovski'),(39,'Aleksandar','Jurišić'),(40,'Andrej','Bauer'),(41,'Alenka','Kavčič'),(42,'Nikolaj','Zimic'),(43,'Iztok','Lebar Bajec'),(44,'Martin','Juvan'),(45,'Igor','Škraba'),(46,'Arjana','Žitnik'),(47,'Miran','Mihelčič'),(48,'Denis','Trček'),(49,'Aleksander','Sadikov'),(50,'Aleš','Leonardis'),(51,'Aleš','Jaklič'),(52,'Neža','Mramor Kosta'),(53,'Tone','Vidmar'),(54,'Bojan','Kverh');
/*!40000 ALTER TABLE `mentor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-06 15:40:32
